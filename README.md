# Docker-Mall

## 安装方法

* 创建两个目录 mkdir -p ~/vol/database ~/vol/web
* 将程序解压到任意目录{git-project-dir}，然后将该工程下资源目录里面文件复制到 ~/vol/web 目录下
* 安装docker、docker-compose
* 下载本工程源码到任意目录
* cd 进入源码目录
* 执行 docker-compose build
* 执行 docker-compose up
* 创建数据库
   - 进入运行中的数据库容器
   - 执行命令 mysql -uroot -p123456 进入数据库
   - 创建数据库： create database dscmall default character set utf8mb4 collate utf8mb4_unicode_ci;
   - 进入创建好的数据库：use dscmall;
   - 设置UTF8： set names utf8;
   - 导入SQL文件：source /{git-project-dir}/数据库/cnmmmcom.sql
* 修改 ~/vol/web/data/config.php 中的数据库配置
* 重新启动一下服务
   - docker-compose down
   - docker-compose up

## 访问地址

* 商城地址：http://IP
* 管理后台：http://IP/admin
